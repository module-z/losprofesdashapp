export const mixShowHidePass = {
    data(){
        return {
            passInputType: 'password',
            showPass: null,
        }
    },
    methods:{
        isShowPass(){
            this.showPass = !this.showPass;
            this.setInputType();
        },
        setInputType(){
            this.showPass ? this.passInputType = 'text' : this.passInputType = 'password';
        }
    },
    computed:{
        showPassIcon(){
            return this.showPass ? 'visibility_off' : 'visibility'
        }
    }

}