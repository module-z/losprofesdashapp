export const mixSignInUpValidate = {
    props:{
        isRegister: {
            type: Boolean,
            default: null,
        }
    },
    data(){
        return {
            validateErrors: [],
            formMsg: null,
            userData: {
                name:'',
                email: '',
                password: ''
            },
            repassword:'',
            validUserData:{
                name:'',
                email: '',
                password: ''
            },
    
            isLoading: null
        }
    },
    methods:{
        redirect(){
            this.$router.push({name: 'Dashboard'});
        },
        validateInit(){
            let emailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            this.userData.email != null && emailReg.test(this.userData.email) ?
            this.validatePass() :
            false
          },
        validatePass(){
            if(this.isRegister){
                this.userData.password === this.repassword ? 
                this.validUserData.password = this.userData.password : 
                this.validateErrors.push('Las contraseñas no coinciden');
                this.validatePassStep2();
            }else{
                this.validatePassStep2();
            }
        
        },
        validatePassStep2(){
            this.userData.password != null && this.userData.password.length >= 8 ?
            this.sanitizeValues() :
            this.validateErrors.push('La contraseña debe tener un mínimo de 8 caracteres');
        },
        sanitizeValues(){
            for(let item in this.userData){
              if(this.userData[item] != null){
                this.validUserData[item] = this.userData[item].replace(/<|>|;|,|{|}|#/gi,'').trim();
              }
            }
            this.isRegister ? this.checkRegister() : this.checkLogin();
        },
          
    },
    computed:{
        
    }

}