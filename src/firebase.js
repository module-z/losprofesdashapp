import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCS6ImoH8D006dyj01S4IpviEcKs3SxBj0",
  authDomain: "losprofesuy.firebaseapp.com",
  databaseURL: "https://losprofesuy.firebaseio.com",
  projectId: "losprofesuy",
  storageBucket: "losprofesuy.appspot.com",
  messagingSenderId: "637481274943",
  appId: "1:637481274943:web:b1f16a8986251a26a2b28a"
};

// const firebaseConfig = {
//   apiKey: "AIzaSyCpQWcY-SzY0WMDOJpXdmdA9sUGNLfMG5Y",
//   authDomain: "halloween-launch.firebaseapp.com",
//   databaseURL: "https://halloween-launch.firebaseio.com",
//   projectId: "halloween-launch",
//   storageBucket: "halloween-launch.appspot.com",
//   messagingSenderId: "1032802962676",
//   appId: "1:1032802962676:web:f9c8d7b6504e860ed362a1"
// };

firebase.initializeApp(firebaseConfig);

export const fb = firebase;
export const db = firebase.firestore();
export const auth = firebase.auth();
export const storage = firebase.storage();

