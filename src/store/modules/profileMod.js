import { db } from "@/firebase.js";
export default {
    namespaced: true,
    state: {
        message: null,
        dbUser: null,
        exists: true
    },
    mutations: {
        mtnMessage(state, n) {
            state.message = n
        },
        mtnDbProfile(state, n) {
            state.dbUser = n
        },
        mtnExists(state, n) {
            state.exists = n
        }
    },
    actions: {
        
        addProfile:({dispatch}, data)=>{
            db.collection('profiles').doc(data[1]).set(data[0])
            .then(() =>{
                console.log('Documento creado con exito');
                dispatch('setMessage', {message:'Documento creado con éxito', type:'success'});
            })
            .catch((error) => {
                console.error("Error adding document: ", error);
                dispatch('setMessage', {message:'Error al crear el documento', type:'error'});
            });
        },

        setMessage: ({commit}, n) => {
            console.log(n);
            commit('mtnMessage', n);
        },

        getProfile: ({commit}, id) => {
            const docRef = db.collection('profiles').doc(id);

            docRef.get().then(function(doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    commit('mtnDbProfile', doc.data());
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                    commit('mtnExists', false)
                }
            }).catch(function(error) {
                console.log("Error getting document:", error);

            });
        },
    },
    getters:{
        getDbUser(state) {
            return state.dbUser
        }
    }
}