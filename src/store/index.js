import Vue from 'vue'
import Vuex from 'vuex'
import userMod from '@/store/modules/userMod'
import profileMod from '@/store/modules/profileMod'

import { auth } from "@/firebase.js";

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    authState({commit}){
      auth.onAuthStateChanged((user)=>{
        user ? commit('userMod/mtnUser', user) : commit('userMod/mtnUser', null)
      })
    }
  },
  modules: {
    userMod,
    profileMod
  }
})

export default store;
store.dispatch('authState');
